'use strict'
const { App } = require('jovo-framework')
const generalHandler = require('./handlers/generalHandler/generalHandler')
const availabilityHandler = require('./handlers/availabilityHandler/availabilityHandler')
const flavourHandler = require('./handlers/flavourHandler/flavourHandler')

const config = {
  logging: true
}

const app = new App(config)

app.setHandler(
  generalHandler,
  availabilityHandler,
  flavourHandler
)

module.exports.app = app
