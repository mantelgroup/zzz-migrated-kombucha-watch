'use strict'
const admin = require('firebase-admin')

admin.initializeApp({
  credential: admin.credential.applicationDefault(),
  databaseURL: 'https://kombucha-watch-e6dae.firebaseio.com'
})

const db = admin.database()
const ref = db.ref()

module.exports = {
  getDatabaseSnapshot: () => {
    return new Promise((resolve, reject) => {
      ref.on('value', (snapshot, error) => {
        if (error) {
          reject(error)
        } else {
          resolve(snapshot.val())
        }
      })
    })
  },
  updateDatabaseValue: (update) => {
    return new Promise((resolve, reject) => {
      ref.update(update, (error) => {
        if (error) {
          reject(error)
        } else {
          resolve()
        }
      })
    })
  }
}
