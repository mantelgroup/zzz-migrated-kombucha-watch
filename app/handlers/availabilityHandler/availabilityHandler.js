'use strict'
const databaseService = require('../../services/databaseService')

module.exports = {
  'GetKombuchaAvailabilityIntent': function () {
    databaseService.getDatabaseSnapshot()
      .then((result) => {
        this.ask(this.t('KOMBUCHA_STATUS', {status: result.currentStatus, prompt: this.t('GENERIC_PROMPT')}), this.t('ASK_ANOTHER'))
      })
      .catch(error => {
        console.log('there was an error', error)
        this.toIntent('ERROR')
      })
  },
  'UpdateKombuchaAvailabilityIntent': function (availability) {
    const newStatus = availability.value
    databaseService.updateDatabaseValue({currentAvailability: newStatus})
      .then(() => {
        this.ask(this.t('UPDATE_KOMBUCHA_STATUS', {status: newStatus, prompt: this.t('GENERIC_PROMPT')}), this.t('ASK_ANOTHER'))
      })
      .catch(error => {
        console.log('there was an error', error)
        this.toIntent('ERROR')
      })
  }
}
