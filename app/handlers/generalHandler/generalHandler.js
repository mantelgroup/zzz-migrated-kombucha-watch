'use strict'

module.exports = {
  'LAUNCH': function () {
    this.ask(this.t('WELCOME'), this.t('ASK_ANOTHER'))
  },
  'END': function () {
    let reason = this.getEndReason()

    console.log(reason)

    this.tell(this.t('END'))
  },
  'ERROR': function () {
    this.ask(this.t('ERROR', {prompt: this.t('GENERIC_PROMPT')}), this.t('ASK_ANOTHER'))
  }
}
