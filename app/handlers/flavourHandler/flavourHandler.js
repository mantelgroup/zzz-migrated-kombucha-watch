'use strict'
const databaseService = require('../../services/databaseService')

module.exports = {
  'GetKombuchaFlavourIntent': function () {
    databaseService.getDatabaseSnapshot()
      .then((result) => {
        this.ask(this.t('KOMBUCHA_FLAVOUR', {flavour: result.currentFlavour, prompt: this.t('GENERIC_PROMPT')}), this.t('ASK_ANOTHER'))
      })
      .catch(error => {
        console.log('there was an error', error)
        this.toIntent('ERROR')
      })
  },
  'UpdateKombuchaFlavourIntent': function (flavour) {
    const newFlavour = flavour.value
    databaseService.updateDatabaseValue({currentFlavour: newFlavour})
      .then(() => {
        this.ask(this.t('UPDATE_KOMBUCHA_FLAVOUR', {flavour: newFlavour, prompt: this.t('GENERIC_PROMPT')}), this.t('ASK_ANOTHER'))
      })
      .catch(error => {
        console.log('there was an error', error)
        this.toIntent('ERROR')
      })
  }
}
