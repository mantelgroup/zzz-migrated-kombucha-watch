# Kombucha Watch

Kombucha watch is a sample Voice assistant integration (app, skill?) that aims to provide the Mantel Group WeWork office the ability to update both the availability and flavour of the Kombucha on Level 8 to decrease the amount of disappointment that is encountered when heading downstairs to find out the Kombucha is out :(.

The main purpose of this project is to provide a sandbox application to test and try out new features with. The project is current using Jovo to build out a cross platform voice application for the Google Assistant and Amazon Alexa.

## Project structure

		.
        ├── index.js              # entry point for the application
        ├── app.json              # application jovo specific configuration
        ├── app                   # code for the application
        │    ├── handlers         # each handler is separated by a different feature
        │    ├── i18n             # i18n language translation strings, used to easily add other supported languages in the future
        │    ├── services         # backend api services (database connection etc.)
        │    └── app.js           # setup for the application (handlers, config etc.)
        ├── platforms             # the actual build code for the respective platforms
        │    ├── alexaSkill       # amazon alexa specific build code
        │    └── googleAction     # google assistant specific build code
        └── models                # locale interaction model code

## Setting up

### Prerequisites
- Access to the Amazon Developer DigIO account in order to deploy the Alexa Skill and authentication set up (see ask-cli)
- Access to the Dialogflow kombucha-watch project in order to deploy the Google Action and authentication set up (see gcloud auth)

### Install

To install the required dependencies
```sh
  yarn install
```

### Run

To run the local webhook version 
```sh
  jovo run
```

Once running, you can then use either the Google Assistant simulator, or the Alexa test simulator in the respective platforms, or connect the same account that has access to the platforms to a Google Home or Amazon Echo (or other variations) device and you should be able to test using the device. (Make sure that testing is enabled for your account in either Dialogflow or Amazon Developer)

If desired, the code can be deployed to Google Cloud Functions or Amazon Lambda (or anywhere) and the api url(fulfillment for Dialogflow, or endpoint for Amazon) will then just need to be updated to point to the correct url endpoint. However as this is just a sample application, running locally should be sufficient unless wanting to actually use the application outside of testing.

## Testing

To run the unit tests and the linting
```sh
  yarn test
```

You can also run the linting standalone
```sh
  yarn run pretest
```

## Deploying

If any updates have been made to the interaction model, need to first build the new platform code models
```sh
  Jovo build
```

To deploy (deploys the Alexa Skill to Amazon Developer and creates a Google Assistant zip file to upload)
```sh
  Jovo deploy
```

To deploy the Google Assistant code without having to upload manually
```sh
  Jovo deploy -p googleAction --project-id kombucha-watch
```

## Built with

- [Jovo](https://github.com/jovotech/jovo-framework-nodejs) - Cross-platform voice application framework
- [Firebase](https://www.npmjs.com/package/firebase-admin) - Used as the database service
- [Jest](https://facebook.github.io/jest/) - Unit testing platform
- [ESLint](https://eslint.org/) - Linting utitlity, specifically using the [standard style](https://standardjs.com/)
- [husky](https://github.com/typicode/husky) - Used for git hooks and running prepush scripts

## TODO

A list of possible TODOs to be done or looked into for the project

- Unit tests and code coverage (Most seem to use Virtual Alexa, but would be good to have a more generic way)
- Actually deploy the code to Google Cloud Functions or Amazon Lambda if desired to actually use the app without having to run locally
- Setup CI/CD
- Integrate with Slack for finding out who wants kombucha
- Integrate Analytics (Chatbase is a great tool by google, but can use others)
- Add in a who fetched the kombucha last intent
- Add in authentication for both amazon alexa and google assistant (Slack integration may need this)
